package com.example.pruebaframent1;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {
    // Declaramos nuestras variables de forma glabal
    // FragmentManager: Pueden agregar, quitar, reemplazar y realizar otras acciones con fregmentos
    // en respuesta a la interacción del usuario. Cada conjunto de cambios de fragmentos que confirma se denomina
    // "transaction" y esto puede identificar que hacer dentro de la transacción.
    // En una "transaction" se puede agrupar varias acciones

    // Cuando esto es util (transaction) cuando tenemos varios fragments mostrados en la misma pantalla.

    FragmentTransaction transaction;
    Fragment fragmentInicio, fragmentRojo, fragmentVerde;

//    FragmentTransaction transaction;
//    InicioFragment fragmentInicio;
//    RojoFragment fragmentRojo;
//    VerdeFragment fragmentVerde;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        fragmentInicio = new InicioFragment();
        fragmentRojo = new RojoFragment();
        fragmentVerde = new VerdeFragment();

        // commit() = Método que devuelve un true cuando confirmamos que ha quedado guardado
        getSupportFragmentManager().beginTransaction().add(R.id.contenedorFragments, fragmentInicio).commit();

    }

    public void onClick(View view){
        // FragmentManager = Pueden agregar, quitar, reemplazar y realizar otras acciones con fragmentos,
        // en respuesta de la interacción del usuario con nuestra app.

        transaction = getSupportFragmentManager().beginTransaction();
        switch (view.getId())
        {
            case R.id.btnRojo: transaction.replace(R.id.contenedorFragments, fragmentRojo);
                transaction.addToBackStack(null); // Cuando le demos en el botón "atras" no nos cierre la app
                break;
            case R.id.btnVerde: transaction.replace(R.id.contenedorFragments, fragmentVerde);
                transaction.addToBackStack(null);
                break;
        }
        transaction.commit();
    }
}